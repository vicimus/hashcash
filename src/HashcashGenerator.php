<?php

namespace Vicimus\Hashcash;

use InvalidArgumentException;

/**
 * Generates Hashcash values based on specific parameters. Used with hashcash
 * requirements of various APIs. Generally associated with Vicimus API Client
 * Registration process.
 *
 * The hashcash takes significant time and processing power to generate (and
 * that time can be increased by the number of bits required) and this prevents
 * spam, abuse, denial of service, etc.
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class HashcashGenerator
{
    /**
     * These are the valid algorthims that can be used to generate hashcash
     * values.
     *
     * @var string[]
     */
    protected $validAlgs = [
        'sha1',
        'md5',
    ];

    /**
     * Generate a new Hashcash value based on the parameters of the method
     *
     * @param string $resource Typically a URL of the resource you are sending
     *                         the hashcash to.
     * @param int    $bits     The number of bits the hashcash must match. The
     *                         higher the number, the more time it will take to
     *                         generate the Hashcash (the higher the cost).
     * @param string $hash     The hash algorithm used to generate the hashcash.
     *
     * @return string
     */
    public function make($resource, $bits = 20, $hash = 'sha1')
    {
        if (!$resource) {
            throw new InvalidArgumentException('Must specify a resource');
        }

        if (!is_int((int)$bits) || $bits <= 0) {
            throw new InvalidArgumentException('$bits must be a pos. integer');
        }

        if (!in_array($hash, $this->validAlgs)) {
            throw new InvalidArgumentException($hash.' is not a valid value');
        }

        $date = (new \DateTime)->format('ymd');
        $rand = bin2hex(openssl_random_pseudo_bytes(6));

        $prefix = sprintf(
            '1:%s:%s:%s::%s:',
            $bits,
            $date,
            $resource,
            $rand
        );

        $counter = 0;

        $n = (int)ceil($bits/4.0);

        $leadZeros = str_pad('', $n, '0');
        
        while (true) {
            $digest = call_user_func($hash, sprintf('%s%x', $prefix, $counter));
            if (substr($digest, 0, $n) === $leadZeros) {
                return sprintf('%s%x', $prefix, $counter);
            }
            
            $counter++;
        }
    }
}
