<?php

namespace Vicimus\Hashcash;

/**
 * Test the basic functionality of the generator
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class HashcashTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the make method to ensure it generates a hashcash
     *
     * @return void
     */
    public function testMake()
    {
        $generator = new HashcashGenerator;
        $hashcash = $generator->make('test.com', 20, 'sha1');

        $this->assertInternalType('string', $hashcash);
    }

    /**
     * Ensure the generator fails in an expected way when an invalid value for
     * bits is used.
     *
     * @expectedException \InvalidArgumentException
     *
     * @return void
     */
    public function testMakeWithInvalidBits()
    {
        $generator = new HashcashGenerator;
        $hashcash = $generator->make('test.com', -1, 'sha1');
    }

    /**
     * Ensure the generator fails in an expected way when an invalid value for
     * bits is used.
     *
     * @expectedException \InvalidArgumentException
     *
     * @return void
     */
    public function testMakeWithInvalidBitType()
    {
        $generator = new HashcashGenerator;
        $hashcash = $generator->make('test.com', 'banana', 'sha1');
    }

    /**
     * Ensure the generator fails in an expected way when an invalid value for
     * alg is used.
     *
     * @expectedException \InvalidArgumentException
     *
     * @return void
     */
    public function testMakeWithInvalidAlgorithm()
    {
        $generator = new HashcashGenerator;
        $hashcash = $generator->make('test.com', 20, 'banana');
    }

    /**
     * Ensure the generator fails in an expected way when an invalid value for
     * resource is used.
     *
     * @expectedException \InvalidArgumentException
     *
     * @return void
     */
    public function testMakeWithInvalidResource()
    {
        $generator = new HashcashGenerator;
        $hashcash = $generator->make(null, 20, 'sha1');
    }
}
